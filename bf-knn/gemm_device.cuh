#ifndef BF_KNN_DEVICE_CUH_
#define BF_KNN_DEVICE_CUH_

#define TILE_SIZE 96
#define CHUNK_SIZE 16
#define BLOCK_SIZE 16

__global__ void kComputeDistances(const int K, const int M, const int N,
                                  const float* A, const float* B, long long* C);

// Knn_NV >= nn
#define K500_NT 64
#define K500_VT 9
#define K500_NV K500_NT * K500_VT
#define K1000_NT 128
#define K1000_VT 11
#define K1000_NV K1000_NT * K1000_VT
#define K2000_NT 256
#define K2000_VT 11
#define K2000_NV K2000_NT * K2000_VT
#define K3000_NT 512
#define K3000_VT 6
#define K3000_NV K3000_NT * K3000_VT

__global__ void kRetrieveResults(const int pnr, const int nn,
                                 const long long* const C, int* const idx,
                                 float* const dist);

#endif  // BF_KNN_DEVICE_CUH_
