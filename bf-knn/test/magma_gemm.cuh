//#include "gemm_device.cuh"


#include "util.h"


#define DIM_X  16
#define DIM_Y  16

// A x B
// size of work for a thread block
//#define BLK_M_nn  96
//#define BLK_N_nn  96
#define BLK_M  96
#define BLK_N  96

#define BLK_K  16

// size of thread block for reading A (dev->regs->shmem)
#define DIM_XA  32
#define DIM_YA  8

// size of thread block for reading B (dev->regs->shmem)
#define DIM_XB  32
#define DIM_YB  8

// size of work for a thread
#define THR_M ( BLK_M / DIM_X )
#define THR_N ( BLK_N / DIM_Y )


#include "kernel_magma_sgemm2.h"

//#define TILE_SIZE  96
//#define CHUNK_SIZE 16
//#define BLOCK_SIZE 16
//
//#define DIM_XAB 32
//#define DIM_YAB 8

#define TILE_SIZE  4
#define CHUNK_SIZE 2
#define BLOCK_SIZE 2

#define DIM_XAB 2
#define DIM_YAB 2

#define THR_NUM ( TILE_SIZE / BLOCK_SIZE )

#define THR_XAB ( TILE_SIZE / DIM_XAB )
#define THR_YAB ( CHUNK_SIZE / DIM_YAB )


__global__ void kernel_magma_sgemm3(const int K, const int M, const int N,
                                  const float* A, const float* B,
								  const float* A2,const float* B2,
								  float* C) {
                                  //long long* C) {
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int tid = ty * BLOCK_SIZE + tx;
  int tx2 = tid % DIM_XAB;
  int ty2 = tid / DIM_XAB;

  volatile __shared__ float as[CHUNK_SIZE][TILE_SIZE];
  volatile __shared__ float bs[CHUNK_SIZE][TILE_SIZE];

  volatile __shared__ float a2s[TILE_SIZE];
  volatile __shared__ float b2s[TILE_SIZE];


  float cr[THR_NUM][THR_NUM];
  float ar;
  float br[THR_NUM];

  float asr[THR_XAB][THR_YAB];
  float bsr[THR_XAB][THR_YAB];

  float a2r;
  float b2r[THR_NUM];

  A += ty2 * M + (by * TILE_SIZE + tx2);
  B += ty2 * N + (bx * TILE_SIZE + tx2);
  C += (by * TILE_SIZE + ty) * N + (bx * TILE_SIZE + tx);



  // Zero C reg
  #pragma unroll
  for (int i = 0; i < THR_NUM; ++i)
    #pragma unroll
    for (int j = 0; j < THR_NUM; ++j) cr[i][j] = 0.0f;



//  // Load A2 gmem->smem
//  #pragma unroll
//  for (int i = 0; i < THR_NUM; ++i)
//	a2s[i] = A2[i*BLOCK_SIZE];
//
//  // Load B2 gmem->smem
//  #pragma unroll
//  for (int i = 0; i < 6; ++i)
//	b2s[i] = B2[i*BLOCK_SIZE];


//  A2 += by * TILE_SIZE + ty;
//  B2 += bx * TILE_SIZE + tx;

  //Load A2 gmem->smem

  //for (blockDim.y * blockDim.x
  
//  if (tid < TILE_SIZE) { // Assume total thread number in a block (16 * 16) is larger than TILE_SIZE (96)
////	a2s[tid] = A2[by * TILE_SIZE + ty + i];
////	b2s[tid] = B2[bx * TILE_SIZE + tx + i];
//	a2s[tid] = A2[by * TILE_SIZE + tid];
//	b2s[tid] = B2[by * TILE_SIZE + tid];
//  }

  #pragma unroll
  for (int i = tid; i < TILE_SIZE; i += blockDim.x * blockDim.y) {
	a2s[i] = A2[by * TILE_SIZE + i];
	b2s[i] = B2[bx * TILE_SIZE + i];
  }

  //__syncthreads();


  // Load A gmem->smem
  #pragma unroll
  for (int i = 0; i < THR_YAB; ++i) {
    #pragma unroll
    for (int j = 0; j < THR_XAB; ++j) as[i * DIM_YAB + ty2][j * DIM_XAB + tx2] = A[j * DIM_XAB];
    A += M * DIM_YAB;
  }

  // Load B gmem->smem
  #pragma unroll
  for (int i = 0; i < THR_YAB; ++i) {
    #pragma unroll
    for (int j = 0; j < THR_XAB; ++j) bs[i * DIM_YAB + ty2][j * DIM_XAB + tx2] = B[j * DIM_XAB];
    B += N * DIM_YAB;
  }

  __syncthreads();

  for (int kk = 0; kk < K - CHUNK_SIZE; kk += CHUNK_SIZE) {
    // Load A gmen->reg
    #pragma unroll
    for (int i = 0; i < THR_YAB; ++i) {
      #pragma unroll
      for (int j = 0; j < THR_XAB; ++j) asr[i][j] = A[j * DIM_XAB];
      A += M * DIM_YAB;
    }

    // Load B gmem->reg
    #pragma unroll
    for (int i = 0; i < THR_YAB; ++i) {
      #pragma unroll
      for (int j = 0; j < THR_XAB; ++j) bsr[i][j] = B[j * DIM_XAB];
      B += N * DIM_YAB;
    }

    // Compute
    #pragma unroll
    for (int k = 0; k < CHUNK_SIZE; ++k) {
      // Load B smen->reg
      #pragma unroll
      for (int j = 0; j < THR_NUM; ++j) br[j] = bs[k][j * BLOCK_SIZE + tx];

      #pragma unroll
      for (int i = 0; i < THR_NUM; ++i) {
        ar = as[k][i * BLOCK_SIZE + ty];
        #pragma unroll
        for (int j = 0; j < THR_NUM; ++j) {
          //float d = ar - br[j];
          //cr[i][j] += d * d;
		  cr[i][j] += ar * br[j];
        }
      }
    }

    __syncthreads();

    // Load A reg->smem
    #pragma unroll
    for (int i = 0; i < THR_YAB; ++i)
      #pragma unroll
      for (int j = 0; j < THR_XAB; ++j) as[i * DIM_YAB + ty2][j * DIM_XAB + tx2] = asr[i][j];

    // Load B reg->smem
    #pragma unroll
    for (int i = 0; i < THR_YAB; ++i)
      #pragma unroll
      for (int j = 0; j < THR_XAB; ++j) bs[i * DIM_YAB + ty2][j * DIM_XAB + tx2] = bsr[i][j];

    __syncthreads();
  }

  // Compute last 16 dimensions
  #pragma unroll
  for (int k = 0; k < CHUNK_SIZE; ++k) {
    // Load B smen->reg
    #pragma unroll
    for (int j = 0; j < THR_NUM; ++j) br[j] = bs[k][j * BLOCK_SIZE + tx];

    #pragma unroll
    for (int i = 0; i < THR_NUM; ++i) {
      ar = as[k][i * BLOCK_SIZE + ty];
      #pragma unroll
      for (int j = 0; j < THR_NUM; ++j) {
        //float d = ar - br[j];
        //cr[i][j] += d * d;
		cr[i][j] += ar * br[j];
      }
    }
  }



  for (int j = 0; j < THR_NUM; ++j) b2r[j] = b2s[j * BLOCK_SIZE + tx];
  // multiply -2
  #pragma unroll
  for (int i = 0; i < THR_NUM; ++i) {
    a2r = a2s[i * BLOCK_SIZE + ty];
    #pragma unroll
    for (int j = 0; j < THR_NUM; ++j) {
	  //b2r = b2s[j * BLOCK_SIZE + tx];
      //float d = ar - br[j];
      //cr[i][j] += d * d;
	  cr[i][j] = -2.0f * cr[i][j] + a2r + b2r[j];
    }
  }



  // Store C reg->gmem
  #pragma unroll
  for (int i = 0; i < THR_NUM; ++i) {
    #pragma unroll
    for (int j = 0; j < THR_NUM; ++j) {
//      long long c = (long long)__float_as_int(cr[i][j]);
//      c = (c << DIM_XAB) | (bx * TILE_SIZE + j * BLOCK_SIZE + tx);
//      C[j * BLOCK_SIZE] = c;

      C[j * BLOCK_SIZE] = cr[i][j];
    }
    C += N * BLOCK_SIZE;
  }

}




__global__ void kernel_magma_sgemm(const int K, const int M, const int N,
                                  const float* A, const float* B,
                                  float* C) {
                                  //long long* C) {
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int tid = ty * BLOCK_SIZE + tx;
  int tx2 = tid % DIM_XAB;
  int ty2 = tid / DIM_XAB;

  volatile __shared__ float as[CHUNK_SIZE][TILE_SIZE];
  volatile __shared__ float bs[CHUNK_SIZE][TILE_SIZE];

  float cr[THR_NUM][THR_NUM];
  float ar;
  float br[THR_NUM];

  float asr[THR_YAB][THR_XAB];
  float bsr[THR_YAB][THR_XAB];

  A += ty2 * M + (by * TILE_SIZE + tx2);
  B += ty2 * N + (bx * TILE_SIZE + tx2);
  C += (by * TILE_SIZE + ty) * N + (bx * TILE_SIZE + tx);

  // Zero C reg
  #pragma unroll
  for (int i = 0; i < THR_NUM; ++i)
    #pragma unroll
    for (int j = 0; j < THR_NUM; ++j) cr[i][j] = 0.0f;

  // Load A gmem->smem
  #pragma unroll
  for (int i = 0; i < THR_YAB; ++i) {
    #pragma unroll
    for (int j = 0; j < THR_XAB; ++j) as[i * DIM_YAB + ty2][j * DIM_XAB + tx2] = A[j * DIM_XAB];
    A += M * DIM_YAB;
  }

  // Load B gmem->smem
  #pragma unroll
  for (int i = 0; i < THR_YAB; ++i) {
    #pragma unroll
    for (int j = 0; j < THR_XAB; ++j) bs[i * DIM_YAB + ty2][j * DIM_XAB + tx2] = B[j * DIM_XAB];
    B += N * DIM_YAB;
  }

  __syncthreads();

  for (int kk = 0; kk < K - CHUNK_SIZE; kk += CHUNK_SIZE) {
    // Load A gmen->reg
    #pragma unroll
    for (int i = 0; i < THR_YAB; ++i) {
      #pragma unroll
      for (int j = 0; j < THR_XAB; ++j) asr[i][j] = A[j * DIM_XAB];
      A += M * DIM_YAB;
    }

    // Load B gmem->reg
    #pragma unroll
    for (int i = 0; i < THR_YAB; ++i) {
      #pragma unroll
      for (int j = 0; j < THR_XAB; ++j) bsr[i][j] = B[j * DIM_XAB];
      B += N * DIM_YAB;
    }

    // Compute
    #pragma unroll
    for (int k = 0; k < CHUNK_SIZE; ++k) {
      // Load B smen->reg
      #pragma unroll
      for (int j = 0; j < THR_NUM; ++j) br[j] = bs[k][j * BLOCK_SIZE + tx];

      #pragma unroll
      for (int i = 0; i < THR_NUM; ++i) {
        ar = as[k][i * BLOCK_SIZE + ty];
        #pragma unroll
        for (int j = 0; j < THR_NUM; ++j) {
          float d = ar - br[j];
          cr[i][j] += d * d;
		  //cr[i][j] += ar * br[j];
        }
      }
    }

    __syncthreads();

    // Load A reg->smem
    #pragma unroll
    for (int i = 0; i < THR_YAB; ++i)
      #pragma unroll
      for (int j = 0; j < THR_XAB; ++j) as[i * DIM_YAB + ty2][j * DIM_XAB + tx2] = asr[i][j];

    // Load B reg->smem
    #pragma unroll
    for (int i = 0; i < THR_YAB; ++i)
      #pragma unroll
      for (int j = 0; j < THR_XAB; ++j) bs[i * DIM_YAB + ty2][j * DIM_XAB + tx2] = bsr[i][j];

    __syncthreads();
  }

  // Compute last 16 dimensions
  #pragma unroll
  for (int k = 0; k < CHUNK_SIZE; ++k) {
    // Load B smen->reg
    #pragma unroll
    for (int j = 0; j < THR_NUM; ++j) br[j] = bs[k][j * BLOCK_SIZE + tx];

    #pragma unroll
    for (int i = 0; i < THR_NUM; ++i) {
      ar = as[k][i * BLOCK_SIZE + ty];
      #pragma unroll
      for (int j = 0; j < THR_NUM; ++j) {
        float d = ar - br[j];
        cr[i][j] += d * d;
		//cr[i][j] += ar * br[j];
      }
    }
  }

  // Store C reg->gmem
  #pragma unroll
  for (int i = 0; i < THR_NUM; ++i) {
    #pragma unroll
    for (int j = 0; j < THR_NUM; ++j) {

      //long long c = (long long)__float_as_int(cr[i][j]);
      //c = (c << DIM_XAB) | (bx * TILE_SIZE + j * BLOCK_SIZE + tx);
	  ////float c = cr[i][j];
      //C[j * BLOCK_SIZE] = c;
      C[j * BLOCK_SIZE] = cr[i][j];
    }
    C += N * BLOCK_SIZE;
  }
}


void magma_sgemm3(const int padded_num_k,
                      const int padded_num_m,
                      const int padded_num_n,
                      const float* const d_padded_AA,
                      const float* const d_padded_BB,
					  const float* const d_padded_AA2,
                      const float* const d_padded_BB2,
                      float* const d_padded_CC) {
  assert(padded_num_k % CHUNK_SIZE == 0);
  assert(padded_num_m % TILE_SIZE == 0);
  assert(padded_num_n % TILE_SIZE == 0);

  dim3 block(BLOCK_SIZE, BLOCK_SIZE);
  dim3 grid(padded_num_n / TILE_SIZE, padded_num_m / TILE_SIZE);

  CREATE_AND_START_TIMER;

  kernel_magma_sgemm3<<<grid, block>>>
      (padded_num_k, padded_num_m, padded_num_n,
       d_padded_AA, d_padded_BB,
       d_padded_AA2, d_padded_BB2,
	   d_padded_CC);

  SYNC_AND_CHECK_ERROR;

  STOP_TIMER_AND_CALCULATE_ELAPSED;

  //if (kPrintTime)
  printf("Time: %.3f ms\n", gpu_timer.elapsed());
}




void magma_sgemm(const int padded_num_k,
                      const int padded_num_m,
                      const int padded_num_n,
                      const float* const d_padded_AA,
                      const float* const d_padded_BB,
                      float* const d_padded_CC) {
  assert(padded_num_k % CHUNK_SIZE == 0);
  assert(padded_num_m % TILE_SIZE == 0);
  assert(padded_num_n % TILE_SIZE == 0);

  dim3 block(BLOCK_SIZE, BLOCK_SIZE);
  dim3 grid(padded_num_n / TILE_SIZE, padded_num_m / TILE_SIZE);

  CREATE_AND_START_TIMER;

  kernel_magma_sgemm<<<grid, block>>>
      (padded_num_k, padded_num_m, padded_num_n,
       d_padded_AA, d_padded_BB, d_padded_CC);

  SYNC_AND_CHECK_ERROR;

  STOP_TIMER_AND_CALCULATE_ELAPSED;

  //if (kPrintTime)
  printf("Time: %.3f ms\n", gpu_timer.elapsed());
}

//inline int CeilToMultiple(int x, int f) {
//  assert(x >= 0);
//  assert(f > 0);
//  return (x + f - 1) / f * f;
//}



void magma_sgemm_pad3(const int num_k, const int num_m,
                         const int num_n,
                         const float* const AA, const float* const BB,
						 float* const CC) {
  assert(num_k > 0);
  assert(num_m > 0);
  assert(num_n > 0);

  assert(AA != NULL);
  assert(BB != NULL);
  assert(CC != NULL);

  // The reason that 'AA' and 'BB' are padded is kComputeDistances
  // only works for a complete 96 X 96 tile of 'CC' and processes a chunk
  // of 16 dimensions in each iteration.

  const int padded_num_m = CeilToMultiple(num_m, TILE_SIZE);
  const int padded_num_n = CeilToMultiple(num_n, TILE_SIZE);
  const int padded_num_k = CeilToMultiple(num_k, CHUNK_SIZE);

  float* h_padded_AA = new float[padded_num_k * padded_num_m];
  float* h_padded_BB = new float[padded_num_k * padded_num_n];
  float* h_padded_CC = new float[padded_num_m * padded_num_n];

  float* d_padded_AA  = NULL;
  CHECK_ERROR( cudaMalloc((void**)&d_padded_AA, sizeof(float) * padded_num_k * padded_num_m));
  float* d_padded_BB  = NULL;
  CHECK_ERROR( cudaMalloc((void**)&d_padded_BB, sizeof(float) * padded_num_k * padded_num_n));
  float* d_padded_CC = NULL;
  CHECK_ERROR( cudaMalloc((void**)&d_padded_CC, sizeof(float) * padded_num_m * padded_num_n));
//  int* d_knn_index = NULL;
//  CHECK_ERROR(cudaMalloc((void**)&d_knn_index,
//                         sizeof(int) * num_m * num_nearest_neighbor));

//  float* d_knn_distance = NULL;
//  CHECK_ERROR(cudaMalloc((void**)&d_knn_distance,
//                         sizeof(float) * num_m * num_nearest_neighbor));

  memset((void*)h_padded_AA, 0,
         sizeof(float) * padded_num_k * padded_num_m);
  for (int i = 0; i < num_k; ++i)
    memcpy(h_padded_AA + padded_num_m * i, AA + num_m * i,
           sizeof(float) * num_m);

  memset((void*)h_padded_BB, 0,
         sizeof(float) * padded_num_k * padded_num_n);
  for (int i = 0; i < num_k; ++i)
    memcpy(h_padded_BB + padded_num_n * i,
           BB + num_n * i, sizeof(float) * num_n);

  CHECK_ERROR(
      cudaMemcpy(d_padded_AA, h_padded_AA,
                 sizeof(float) * padded_num_k * padded_num_m,
                 cudaMemcpyHostToDevice));
  CHECK_ERROR(
      cudaMemcpy(d_padded_BB, h_padded_BB,
                 sizeof(float) * padded_num_k * padded_num_n,
                 cudaMemcpyHostToDevice));


  float* h_padded_AA2 = new float[padded_num_m];
  float* h_padded_BB2 = new float[padded_num_n];
  memset((void*)h_padded_AA2, 0, sizeof(float) * padded_num_m);
  memset((void*)h_padded_BB2, 0, sizeof(float) * padded_num_n);

  //printf("AA2:\n");
  for (int i = 0; i < padded_num_m; ++i) {
	for (int k = 0; k < num_k; ++k) {
	  h_padded_AA2[i] += h_padded_AA[k * padded_num_m + i] * h_padded_AA[k * padded_num_m + i];
	}
	//printf("%f\n", h_padded_AA2[i]);
  }
  //printf("BB2:\n");
  for (int i = 0; i < padded_num_n; ++i) {
	for (int k = 0; k < num_k; ++k) {
	  h_padded_BB2[i] += h_padded_BB[k * padded_num_n + i] * h_padded_BB[k * padded_num_n + i];
	}
	//printf("%f\n", h_padded_BB2[i]);
  }


  float* d_padded_AA2 = NULL;
  CHECK_ERROR(
      cudaMalloc((void**)&d_padded_AA2,
                 sizeof(float) * padded_num_m));
  float* d_padded_BB2 = NULL;
  CHECK_ERROR(
      cudaMalloc((void**)&d_padded_BB2,
                 sizeof(float) * padded_num_n));

  CHECK_ERROR(
      cudaMemcpy(d_padded_AA2, h_padded_AA2,
                 sizeof(float) * padded_num_m,
                 cudaMemcpyHostToDevice));
  CHECK_ERROR(
      cudaMemcpy(d_padded_BB2, h_padded_BB2,
                 sizeof(float) * padded_num_n,
                 cudaMemcpyHostToDevice));



  magma_sgemm3(padded_num_k, padded_num_m, padded_num_n,
                   d_padded_AA, d_padded_BB,
                   d_padded_AA2, d_padded_BB2,
				   d_padded_CC);

//  SortCandidateGroups(num_m, num_n, padded_num_n,
//                      num_nearest_neighbor, d_CC);
//
//  MergeCandidateGroups(num_m, num_n, padded_num_n,
//                       num_nearest_neighbor, d_CC);
//
//  RetrieveResults(num_m, padded_num_n, num_nearest_neighbor,
//                  d_CC, d_knn_index, d_knn_distance);

//  CHECK_ERROR(cudaMemcpy(knn_index, d_knn_index,
//                         sizeof(int) * num_m * num_nearest_neighbor,
//                         cudaMemcpyDeviceToHost));
//  CHECK_ERROR(cudaMemcpy(knn_distance, d_knn_distance,
//                         sizeof(float) * num_m * num_nearest_neighbor,
//                         cudaMemcpyDeviceToHost));


  CHECK_ERROR(cudaMemcpy(h_padded_CC, d_padded_CC,
                         sizeof(float) * padded_num_m * padded_num_n,
                         cudaMemcpyDeviceToHost));

  for (int i = 0; i < num_k; ++i)
    memcpy( CC + num_n * i, h_padded_CC + padded_num_n * i,
            sizeof(float) * num_n);


  delete[] h_padded_AA;
  delete[] h_padded_BB;
  delete[] h_padded_CC;

  CHECK_ERROR(cudaFree(d_padded_AA));
  CHECK_ERROR(cudaFree(d_padded_BB));
  CHECK_ERROR(cudaFree(d_padded_CC));
}


void magma_sgemm_pad(const int num_k, const int num_m,
                         const int num_n,
                         const float* const AA, const float* const BB,
						 float* const CC) {
  assert(num_k > 0);
  assert(num_m > 0);
  assert(num_n > 0);

  assert(AA != NULL);
  assert(BB != NULL);
  assert(CC != NULL);

  // The reason that 'AA' and 'BB' are padded is kComputeDistances
  // only works for a complete 96 X 96 tile of 'CC' and processes a chunk
  // of 16 dimensions in each iteration.

  const int padded_num_m = CeilToMultiple(num_m, TILE_SIZE);
  const int padded_num_n = CeilToMultiple(num_n, TILE_SIZE);
  const int padded_num_k = CeilToMultiple(num_k, CHUNK_SIZE);

  float* h_padded_AA = new float[padded_num_k * padded_num_m];
  float* h_padded_BB = new float[padded_num_k * padded_num_n];
  float* h_padded_CC = new float[padded_num_m * padded_num_n];

  float* d_padded_AA  = NULL;
  CHECK_ERROR( cudaMalloc((void**)&d_padded_AA, sizeof(float) * padded_num_k * padded_num_m));
  float* d_padded_BB  = NULL;
  CHECK_ERROR( cudaMalloc((void**)&d_padded_BB, sizeof(float) * padded_num_k * padded_num_n));
  float* d_padded_CC = NULL;
  CHECK_ERROR( cudaMalloc((void**)&d_padded_CC, sizeof(float) * padded_num_m * padded_num_n));
//  int* d_knn_index = NULL;
//  CHECK_ERROR(cudaMalloc((void**)&d_knn_index,
//                         sizeof(int) * num_m * num_nearest_neighbor));

//  float* d_knn_distance = NULL;
//  CHECK_ERROR(cudaMalloc((void**)&d_knn_distance,
//                         sizeof(float) * num_m * num_nearest_neighbor));

  memset((void*)h_padded_AA, 0,
         sizeof(float) * padded_num_k * padded_num_m);
  for (int i = 0; i < num_k; ++i)
    memcpy(h_padded_AA + padded_num_m * i, AA + num_m * i,
           sizeof(float) * num_m);

  memset((void*)h_padded_BB, 0,
         sizeof(float) * padded_num_k * padded_num_n);
  for (int i = 0; i < num_k; ++i)
    memcpy(h_padded_BB + padded_num_n * i,
           BB + num_n * i, sizeof(float) * num_n);

  CHECK_ERROR(
      cudaMemcpy(d_padded_AA, h_padded_AA,
                 sizeof(float) * padded_num_k * padded_num_m,
                 cudaMemcpyHostToDevice));
  CHECK_ERROR(
      cudaMemcpy(d_padded_BB, h_padded_BB,
                 sizeof(float) * padded_num_k * padded_num_n,
                 cudaMemcpyHostToDevice));

  magma_sgemm(padded_num_k, padded_num_m, padded_num_n,
                   d_padded_AA, d_padded_BB, d_padded_CC);

//  SortCandidateGroups(num_m, num_n, padded_num_n,
//                      num_nearest_neighbor, d_CC);
//
//  MergeCandidateGroups(num_m, num_n, padded_num_n,
//                       num_nearest_neighbor, d_CC);
//
//  RetrieveResults(num_m, padded_num_n, num_nearest_neighbor,
//                  d_CC, d_knn_index, d_knn_distance);

//  CHECK_ERROR(cudaMemcpy(knn_index, d_knn_index,
//                         sizeof(int) * num_m * num_nearest_neighbor,
//                         cudaMemcpyDeviceToHost));
//  CHECK_ERROR(cudaMemcpy(knn_distance, d_knn_distance,
//                         sizeof(float) * num_m * num_nearest_neighbor,
//                         cudaMemcpyDeviceToHost));


  CHECK_ERROR(cudaMemcpy(h_padded_CC, d_padded_CC,
                         sizeof(float) * padded_num_m * padded_num_n,
                         cudaMemcpyDeviceToHost));

  for (int i = 0; i < num_k; ++i)
    memcpy( CC + num_n * i, h_padded_CC + padded_num_n * i,
            sizeof(float) * num_n);


  delete[] h_padded_AA;
  delete[] h_padded_BB;
  delete[] h_padded_CC;

  CHECK_ERROR(cudaFree(d_padded_AA));
  CHECK_ERROR(cudaFree(d_padded_BB));
  CHECK_ERROR(cudaFree(d_padded_CC));
}




//__global__ void kRetrieveResults(const int pnr, const int nn,
//                                 const long long* const C, int* const idx,
//                                 float* const dist) {
//  int qid = blockIdx.y;
//  int tid = blockIdx.x * blockDim.x + threadIdx.x;
//
//  if (tid >= nn) return;
//
//  long long c = C[qid * pnr + tid];
//  idx[qid * nn + tid] = c & 0xFFFFFFFF;
//  dist[qid * nn + tid] = sqrt(__int_as_float(c >> DIM_XAB));
//}
