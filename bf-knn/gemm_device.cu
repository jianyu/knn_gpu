#include "gemm_device.cuh"

__global__ void kComputeDistances(const int K, const int M, const int N,
                                  const float* A, const float* B,
                                  const float* C) {
                                  //long long* C) {
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int tid = ty * 16 + tx;
  int tx2 = tid % 32;
  int ty2 = tid / 32;

  volatile __shared__ float as[16][96];
  volatile __shared__ float bs[16][96];

  float cr[6][6];
  float ar;
  float br[6];

  float asr[2][3];
  float bsr[2][3];

  A += ty2 * M + (by * 96 + tx2);
  B += ty2 * N + (bx * 96 + tx2);
  C += (by * 96 + ty) * N + (bx * 96 + tx);

  // Zero C reg
  #pragma unroll
  for (int i = 0; i < 6; ++i)
    #pragma unroll
    for (int j = 0; j < 6; ++j) cr[i][j] = 0.0f;

  // Load A gmem->smem
  #pragma unroll
  for (int i = 0; i < 2; ++i) {
    #pragma unroll
    for (int j = 0; j < 3; ++j) as[i * 8 + ty2][j * 32 + tx2] = A[j * 32];
    A += M * 8;
  }

  // Load B gmem->smem
  #pragma unroll
  for (int i = 0; i < 2; ++i) {
    #pragma unroll
    for (int j = 0; j < 3; ++j) bs[i * 8 + ty2][j * 32 + tx2] = B[j * 32];
    B += N * 8;
  }

  __syncthreads();

  for (int kk = 0; kk < K - 16; kk += 16) {
    // Load A gmen->reg
    #pragma unroll
    for (int i = 0; i < 2; ++i) {
      #pragma unroll
      for (int j = 0; j < 3; ++j) asr[i][j] = A[j * 32];
      A += M * 8;
    }

    // Load B gmem->reg
    #pragma unroll
    for (int i = 0; i < 2; ++i) {
      #pragma unroll
      for (int j = 0; j < 3; ++j) bsr[i][j] = B[j * 32];
      B += N * 8;
    }

    // Compute
    #pragma unroll
    for (int k = 0; k < 16; ++k) {
      // Load B smen->reg
      #pragma unroll
      for (int j = 0; j < 6; ++j) br[j] = bs[k][j * 16 + tx];

      #pragma unroll
      for (int i = 0; i < 6; ++i) {
        ar = as[k][i * 16 + ty];
        #pragma unroll
        for (int j = 0; j < 6; ++j) {
          //float d = ar - br[j];
          //cr[i][j] += d * d;
		  cr[i][j] += ar * br[j];
        }
      }
    }

    __syncthreads();

    // Load A reg->smem
    #pragma unroll
    for (int i = 0; i < 2; ++i)
      #pragma unroll
      for (int j = 0; j < 3; ++j) as[i * 8 + ty2][j * 32 + tx2] = asr[i][j];

    // Load B reg->smem
    #pragma unroll
    for (int i = 0; i < 2; ++i)
      #pragma unroll
      for (int j = 0; j < 3; ++j) bs[i * 8 + ty2][j * 32 + tx2] = bsr[i][j];

    __syncthreads();
  }

  // Compute last 16 dimensions
  #pragma unroll
  for (int k = 0; k < 16; ++k) {
    // Load B smen->reg
    #pragma unroll
    for (int j = 0; j < 6; ++j) br[j] = bs[k][j * 16 + tx];

    #pragma unroll
    for (int i = 0; i < 6; ++i) {
      ar = as[k][i * 16 + ty];
      #pragma unroll
      for (int j = 0; j < 6; ++j) {
        //float d = ar - br[j];
        //cr[i][j] += d * d;
		cr[i][j] += ar * br[j];
      }
    }
  }

  // Store C reg->gmem
  #pragma unroll
  for (int i = 0; i < 6; ++i) {
    #pragma unroll
    for (int j = 0; j < 6; ++j) {

      //long long c = (long long)__float_as_int(cr[i][j]);
      //c = (c << 32) | (bx * 96 + j * 16 + tx);
	  ////float c = cr[i][j];
      //C[j * 16] = c;
      C[j * 16] = cr[i][j];
    }
    C += N * 16;
  }
}


__global__ void kRetrieveResults(const int pnr, const int nn,
                                 const long long* const C, int* const idx,
                                 float* const dist) {
  int qid = blockIdx.y;
  int tid = blockIdx.x * blockDim.x + threadIdx.x;

  if (tid >= nn) return;

  long long c = C[qid * pnr + tid];
  idx[qid * nn + tid] = c & 0xFFFFFFFF;
  dist[qid * nn + tid] = sqrt(__int_as_float(c >> 32));
}
